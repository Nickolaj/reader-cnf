<?php

namespace ConfigReader\ConfReader;

class ExtendConf {
    private $conf;

    public function __construct(ContentConf $conf){
        $this->conf = $conf;
    }

    function run(string $firstArgument, string $secondArgument) {
        $nameClass = HelperConf::makeClassName('conf', $firstArgument);
        echo $nameClass . PHP_EOL;

        if (file_exists(HelperConf::makeAbsoluteFileName($nameClass, 'php'))) {
            echo 'exist:' . $nameClass . PHP_EOL. PHP_EOL;
        } else {
            echo 'It does not exist: ' . $nameClass . PHP_EOL. PHP_EOL;
            exit(1);
        }

        //$path = [$nameClass, 'run'];
        $value = (new $nameClass($this->conf))->run($secondArgument);
        echo gettype($value)
            //. '=>'
            //. print_r($value, 1)
            . PHP_EOL
        ;
        return $value;
    }
}
