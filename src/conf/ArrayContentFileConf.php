<?php

namespace ConfigReader\Conf;

class ArrayContentFileConf {
    function toArray($content): array {
        return $content;
    }

    function toObject($content): object {
        return (object) $content;
    }
}
