<?php

namespace ConfigReader\ConfExtend;

class ConfDefault {
    //@todo make two class - default and custom
    private $type = 'string';
    private $conf;

    public function __construct(ContentConf $confContent) {
        $this->conf = $confContent;
    }

    public function setType(string $type): self {
        $this->type = $type;
        return $this;
    }

    public function getType(): string {
        return $this->type;
    }

    function run(string $field = '-') {
        DebugConf::printEOLmltML(__METHOD__, __LINE__, $field);

        $val = $this->get($field, '');

        assert(property_exists(self::class, 'type'));
        assert(!is_null($this->type));
        return $this->convertTo($this->type, $val);
    }

    function get(string $field, $default) {
        return $this->conf->get($field, $default);
    }

    function convertTo(string $typeName, $value) {
        if ($this->isString($typeName)) {
            return (string) $value;
        } elseif ($this->isInteger($typeName)) {
            return (int) $value;
        }
        return $value;
    }

    function isInteger(string $typeName): bool {
        $is = in_array(strtolower($typeName), ['int', 'integer']);
        DebugConf::printEOLml(__METHOD__, ($is ? 'yes': 'no'));
        return $is;
    }

    function isString(string $typeName): bool {
        $is = in_array(strtolower($typeName), ['str', 'string']);
        DebugConf::printEOLml(__METHOD__, ($is ? 'yes': 'no'));
        return $is;
    }
}
