<?php

namespace ConfigReader\ConfExtend;

class HelperConf {
    static function makeAbsoluteFileName(string $name, string $extension): string {
        $dir = dirname(__FILE__);
        $file = sprintf("%s.%s", $name, $extension);
        $pathFile = self::pathJoin([$dir, $file]);

        return $pathFile;
    }

    static function pathJoin(array $parts): string {
        return join(DIRECTORY_SEPARATOR, $parts);
    }

    static function makeClassName(string $name, string $postfix): string {
        $nameClass = sprintf(
            "%s%s",
            ucfirst(strtolower($name)),
            ucfirst(strtolower($postfix))
        );

        return $nameClass;
    }

    static function existByKeyOrException(int $key, $array) {
        if (!key_exists($key, $array)) {
            throw new InvalidArgumentException('It does not have second index!');
        }
        return $array[ $key ];
    }
}
