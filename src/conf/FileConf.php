<?php

namespace ConfigReader\Conf;

use ConfigReader\DebugConf;
use InvalidArgumentException;

class FileConf {
    protected $fileConf;
    protected $formatConf;

    public function __construct(string $fileName, FormatConf $formatConf) {
        $this->formatConf = $formatConf;
        if ($formatConf->isJson()) {
            DebugConf::printEOLmltML(__METHOD__, __LINE__, 'isJson');
            $this->fileConf = new JsonFileConf($fileName);
        } elseif ($formatConf->isArray()) {
            DebugConf::printEOLmltML(__METHOD__, __LINE__, 'isArray');
            $this->fileConf = new ArrayFileConf($fileName);
        } else {
            throw new InvalidArgumentException('Format config uncorrected!');
        }
    }

    function getContent() {
        DebugConf::printEOLml(__METHOD__, __LINE__);
        $content = $this->fileConf->getContent();
        //print_r([__LINE__ => gettype($content)]);
        //print_r([__LINE__ => sizeof($content)]);
        //print_r([__LINE__ => strlen($content)]);
        return $content;
    }

    function getFormatConf() {
        return $this->formatConf;
    }
}
