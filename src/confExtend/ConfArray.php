<?php

namespace ConfigReader\ConfExtend;

class ConfArray {
    private $confExtend;

    public function __construct(ContentConf $confContent) {
        $type = 'array';
        $confExtend = (new ConfDefault($confContent))
                ->setType($type);
        $this->confExtend = $confExtend;
    }

    function run(string $field = '-') {
        $value = $this->confExtend->get($field, [])->toObject();

        $type = $this->confExtend->getType();

        DebugConf::printEOLmltML(__METHOD__, __LINE__, $type);

        $convertTo = $this->confExtend->convertTo($type, $value);
        DebugConf::print(print_r($convertTo, 1));
        $convertTo = $this->convertTo($type, $value);
        DebugConf::print(print_r($convertTo, 1));
        return $convertTo;
    }

    function convertTo(string $typeName, $value) {
        if ($this->isArray($typeName)) {
            return (array)$value;
        }
        return $value;
    }

    function isArray(string $typeName): bool {
        $is = in_array(strtolower($typeName), ['arr', 'array']);
        DebugConf::printEOLml(__METHOD__, ($is ? 'yes': 'no'));
        return $is;
    }
}
