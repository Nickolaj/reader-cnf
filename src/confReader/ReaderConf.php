<?php

namespace ConfigReader\ConfReader;

use ConfigReader\Conf\ContentConf;
use ConfigReader\Conf\FileConf;
use ConfigReader\Conf\FormatConf;
use ConfigReader\ConfExtend\HelperConf;
use ConfigReader\DebugConf;
use InvalidArgumentException;

class ReaderConf {
    private $conf;

    public function __construct(string $nameFile) {
        $nameFormat = (new NameFormatConf($nameFile))->take();
        DebugConf::printEOLmltML(__METHOD__, __LINE__, $nameFormat);

        $formatConf = new FormatConf($nameFormat);
        if (!$formatConf->isSupported()) {
            $message = sprintf('FormatConf(%s) is uncorrected!', $nameFormat);
            throw new InvalidArgumentException($message);
        }

        $this->conf = new ContentConf(new FileConf($nameFile, $formatConf));
    }

    public function path(string $fieldName): string {
        $parts = $this->conf->get($fieldName, [])->toArray();
        $path = HelperConf::pathJoin($parts);
        return $path;
    }

    public function str(string $fieldName): string {
        return $this->conf->get($fieldName, '');
    }

    /**
     * @param $name
     * @param $arguments
     * @return array|ObjectOrArray
     */
    public function __call($name, $arguments) {
        DebugConf::printEOLmltML(__METHOD__, __LINE__,
            DebugConf::nameAndArguments($name, $arguments)
        );

        $firstArgument = HelperConf::existByKeyOrException($indexFirstDefault = 0, $arguments);

        if ('array' == strtolower($name)) {
            return $this->conf->get($firstArgument, [])->toArray();
        } elseif ('string' == strtolower($name)) {
            return $this->conf->get($firstArgument, '');
        } elseif ('ext' == strtolower($name)) {

            $secondArgument = HelperConf::existByKeyOrException($indexSecondDefault = 1, $arguments);

            return (new ExtendConf($this->conf))->run($firstArgument, $secondArgument);
        }

        return null;
    }
}
