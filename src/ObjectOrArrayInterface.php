<?php

namespace ConfigReader;

interface ObjectOrArrayInterface {
    function toArray(): array ;
    function toObject(): object;
    //function likeString(): string;
}
