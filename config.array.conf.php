<?php

return
[
    'log' => [
        'path' =>'/log',
        'file' => 'log.txt'
    ],
    'php_eol' => PHP_EOL,
    'eol' => "",
    't' => "-\r\n-",
    'str' => 'str-ing',
    'integer' => "12321"
];
