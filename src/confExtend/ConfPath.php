<?php

namespace ConfigReader\ConfExtend;

class ConfPath {
    private $confExtend;

    public function __construct(ContentConf $confContent) {
        $confExtend = new ConfDefault($confContent);
        $this->confExtend = $confExtend;
    }

    function run(string $field) {
        DebugConf::printEOLmltML(__METHOD__, __LINE__, $field);

        $parts = $this->confExtend->get($field, [])->toArray();
        $path = HelperConf::pathJoin($parts);

        $type = $this->confExtend->getType();
        return $this->confExtend->convertTo($type, $path);
    }
}
