<?php

namespace ConfigReader\ConfExtend;

class ConfInteger {
    private $confExtend;

    public function __construct(ContentConf $confContent) {
        $type = 'integer';
        $confExtend = (new ConfDefault($confContent))
                ->setType($type);
        $this->confExtend = $confExtend;
    }

    function run(string $field = '-') {
        return $this->confExtend->run($field);
    }
}
