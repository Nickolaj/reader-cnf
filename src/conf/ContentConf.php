<?php

namespace ConfigReader\Conf;

use ConfigReader\ObjectOrArray;

class ContentConf
{
    private $contentFile;
    private $contentFileConf;

    public function __construct(FileConf $fileConf) {
        $this->contentFile = $fileConf->getContent();
        $format = $fileConf->getFormatConf();
        //print_r([__LINE__ => $format->isArray()?'is-array':'is-json']);
        if ($format->isArray()) {
            $this->contentFileConf  = new ArrayContentFileConf();
        } elseif ($format->isJson()) {
            $this->contentFileConf  = new JsonContentFileConf();
        } else {
            throw new InvalidArgumentException('Format is uncorrected!');
        }
    }

    protected function toObject() {
        return $this->contentFileConf->toObject($this->contentFile);
    }

    protected function toArray() {
        return $this->contentFileConf->toArray($this->contentFile);
    }

    public function get(string $field, $default) {
        $valueOfField = $default;
        if ($this->useObject()) {
            //print_r(__LINE__);
            $objectContent = $this->toObject();
            if (!property_exists($objectContent, $field)) {
                print_r([__LINE__ => 'error:' . $field]);
            }
            $valueOfField = $objectContent->$field;
        } else {
            //print_r(__LINE__);
            $objectContent = $this->toArray();
            $valueOfField = $objectContent[$field];
        }

        if (is_string($valueOfField)) {
            return $valueOfField;
        }
        return new ObjectOrArray($valueOfField);
    }

    protected function useObject(): bool {
        $currentPHPVersion = PHP_VERSION_ID;
        $AfterTheVersionUseObject = 70000;
        return ($AfterTheVersionUseObject < $currentPHPVersion);
    }
}
