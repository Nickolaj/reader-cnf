<?php

namespace ConfigReader\Conf;

use ConfigReader\DebugConf;
use InvalidArgumentException;

class FormatConf {
    const JSON = 0;
    const ARRAY = 1;

    private $current;

    public function __construct($format) {
        if (is_string($format)) {
            assert(!is_bool($format));
            //$format = $this->extensionToFormatOrException($format);
            $format = $this->extensionToFormat($format) ?: $format;
            assert(!is_bool($format));
            DebugConf::printEOLmltML(__METHOD__, __LINE__, $format);
        }
        $this->current = $format;
    }

    function isJson(): bool {
        return self::JSON === $this->current;
    }

    function isArray(): bool {
        return self::ARRAY === $this->current;
    }

    function extensionToFormat(string $extension) {
        $ext = strtolower($extension);
        $searched = array_search($ext, $this->formats());

        $dbgMessage = !$searched ? 'is does not found!' : 'is founded!';
        DebugConf::printEOLmltML(__METHOD__, __LINE__,
            $extension .'~['.$searched.'] '. $dbgMessage
        );

        return $searched;
    }

    function extensionToFormatOrException(string $extension) {
        $searched = $this->extensionToFormat($extension);
        if (!$searched) {
            $message = sprintf('Extension for FormatConf(%s) is unsupported!', $extension);
            throw new InvalidArgumentException($message);
        }
        return $searched;
    }

    function isSupported() {
        DebugConf::printEOLmltML(__METHOD__, __LINE__,
            $this->current
        );

        return array_key_exists($this->current, $this->formats());
        //return isset($this->formats()[ $this->current ]);
    }

    final function formats(): array {
        return [
            self::JSON => 'json',
            self::ARRAY => 'array'
        ];
    }
}
