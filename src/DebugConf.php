<?php

namespace ConfigReader;

class DebugConf {

    static public $isDebug = false;

    static function methodAndLine(string $method, string $line): string {
        $delimiter = '::';
        return sprintf('%s%s%s', $method, $delimiter, $line);
    }

    static function nameAndArguments(string $name, array $arguments): string {
        $delimiter = '~';
        $glue = ':';
        $joined = join($glue, $arguments);
        return sprintf('%s%s%s', $name, $delimiter, $joined);
    }

    static function multi(string $methodAndLine, string $nameWithArguments): string {
        $delimiter = ' | ';
        return sprintf('%s%s%s', $methodAndLine, $delimiter, $nameWithArguments);
    }

    static function eol(string $message): string {
        if (!self::$isDebug) {
            return '';
        }

        $delimiter = '';
        return sprintf('%s%s%s', $message, $delimiter, PHP_EOL);
    }

    static function print(string $message) {
        if (!self::$isDebug) {
            print_r('');
        }

        print_r($message);
    }

    static function printEOLmltML(string $m, string $l, $msg) {
        DebugConf::print(DebugConf::eol(DebugConf::multi(DebugConf::methodAndLine($m, $l), $msg)));
    }

    static function printEOLml(string $m, string $l) {
        DebugConf::print(DebugConf::eol(DebugConf::methodAndLine($m, $l)));
    }
}
