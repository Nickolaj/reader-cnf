<?php

namespace ConfigReader\ConfReader;

class NameFormatConf {
    private $nameFile;

    public function __construct(string $nameFile) {
        $this->nameFile = $nameFile;
    }

    function take(): string {
        $format = explode('.', $this->nameFile);
        $nameFormat = $format[1];

        return $nameFormat;
    }
}
