<?php

namespace ConfigReader\Conf;

class JsonContentFileConf {
    static function toArray($content): array {
        $decoded = json_decode($content, true);
        return $decoded;
    }

    static function toObject($content): object {
        $decoded = json_decode($content, false);
        return $decoded;
    }
}
