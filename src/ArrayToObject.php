<?php

namespace ConfigReader;

class ArrayToObject implements ObjectOrArrayInterface{
    private $content;

    public function __construct($content) {
        $this->content = $content;
    }

    function toArray(): array {
        return $this->content;
    }

    function toObject(): object {
        return (object) $this->content;
    }
}
