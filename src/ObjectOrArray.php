<?php

namespace ConfigReader;

class ObjectOrArray implements ObjectOrArrayInterface {
    private $content;

    public function __construct($content) {
        if (is_array($content)) {
            $this->content = new ArrayToObject($content);
        } elseif (is_object($content)) {
            $this->content = new ObjectToArray($content);
        } else {
            throw new InvalidArgumentException(gettype($content) . ' - format is unsupported!');
        }
    }

    function toArray(): array {
        return $this->content->toArray();
    }

    function toObject(): object {
        return $this->content->toObject();
    }
}
