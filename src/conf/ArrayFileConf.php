<?php

namespace ConfigReader\Conf;

class ArrayFileConf {
    private $nameFile;

    public function __construct(string $nameFile) {
        $this->nameFile = $nameFile;
    }

    function content() {
        return require_once($this->nameFile);
    }

    function getContent() {
        return $this->content();
    }
}
