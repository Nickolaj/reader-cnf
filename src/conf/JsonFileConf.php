<?php

namespace ConfigReader\Conf;

class JsonFileConf {
    private $nameFile;

    public function __construct(string $nameFile) {
        $this->nameFile = $nameFile;
    }

    function content() {
        return file_get_contents($this->nameFile);
    }

    function getContent() {
        return $this->content();
    }
}
